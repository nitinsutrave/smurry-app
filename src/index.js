import React from 'react';
import ReactDOM from 'react-dom';

import Smurry from './smurry';

import 'bootstrap/dist/css/bootstrap.min.css';
import './index.css';

ReactDOM.render(
  <Smurry />,
  document.getElementById('root')
);
