import React, { Component } from 'react'
import * as _ from 'lodash'

import Login from './components/Login/Login'
import firebase from './utils/firebase'
import {getRandomTiles} from './gameUtils/boardUtils'

import Container from 'react-bootstrap/Container';
import Loader from './components/Loader/Loader';
import Header from './components/Header/Header';
import Message from './components/Message/Message';
import StartGame from './components/StartGame/StartGame';
import Game from './components/Game/Game'

const getNewGameId = firebase.functions().httpsCallable('getNewGameId');
const joinGameByName = firebase.functions().httpsCallable('joinGameByName');

class Smurry extends Component {
  constructor () {
    super()

    const values = getRandomTiles(12)

    const tiles = {}
    for (let i=0; i<24; i++) tiles[i] = {
      id: i,
      value: values[i],
      text: '',
      isSelected: false,
      isOpened: false,
    }

    this.state = {
      gameName: null,
      gameId: null,
      authenticated: false,
      ready: false,
      frozen: true,
      totalTileCount: 24,
      tiles: tiles,
      selectedTileCount: 0,
      openTileCount: 0,
      prevMove: '',
      userAlert: '',
      userMessage: '',
      swapsLeft: 5,
      userNo: 0,
      user1Score: 0,
      user2Score: 0,
      loaderText: 'Checking for authentication'
    }

    this.dbRef = null
  }

  componentDidMount () {
    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        // User is signed in.

        // Uncomment to debug
        // console.log(user.displayName);
        // console.log(user.email);
        // console.log(user.emailVerified);
        // console.log(user.photoURL);
        // console.log(user.isAnonymous);
        // console.log(user.uid);
        // console.log(user.providerData);

        this.setState({
          authenticated: true,
          loaderText: ''
        })
      } else {
        // User is signed out.
        this.setState({
          authenticated: false,
          loaderText: ''
        })
      }
    });
  }

  genericSetState = (newState) => {
    this.setState(newState)
  }

  handleOpponentMove = (dbSnapshot) => {
    // Uncomment to debug
    // console.log('received a move!')
    // console.log(dbSnapshot)

    // This is the first initialisation call
    if (!dbSnapshot.prevMove) return

    let prevMoveMessage = ''
    if (firebase.auth().currentUser.uid === dbSnapshot.prevMove.player) {
      prevMoveMessage = 'You'
    } else {
      prevMoveMessage = 'Opponent'
    }

    if ('opened' === dbSnapshot.prevMove.move) {
      prevMoveMessage = `${prevMoveMessage} opened tiles`
    } else {
      prevMoveMessage = `${prevMoveMessage} swapped tiles`
    }

    // Only set the previous move message and return
    // as we don't want to perform our own move again
    if (firebase.auth().currentUser.uid === dbSnapshot.prevMove.player) {
      this.setState({
        prevMove: prevMoveMessage,
      })
      return
    }

    this.setState({
      totalTileCount: dbSnapshot.totalTileCount,
      tiles: dbSnapshot.tiles,
      selectedTileCount: dbSnapshot.selectedTileCount,
      openTileCount: dbSnapshot.openTileCount,
      prevMove: prevMoveMessage,
      user1Score: dbSnapshot.user1Score || 0,
      user2Score: dbSnapshot.user2Score || 0,
      userMessage: 'Your move..',
    }, () => {
      if ('opened' === dbSnapshot.prevMove.move) {
        this.performShowTile(false)
      } else {
        this.performSwapTile(false)
      }
    })
  }

  startNewGame = () => {
    this.setState({
      loaderText: 'Starting Game'
    })

    getNewGameId({
      totalTileCount: this.state.totalTileCount,
      tiles: this.state.tiles,
      selectedTileCount: this.state.selectedTileCount,
      openTileCount: this.state.openTileCount,
      prevMove: null
    }).then((result) => {
        // Read result of the Cloud Function.
        this.setState({
          gameId: result.data.gameId,
          gameName: result.data.gameName,
          userNo: 1,
          loaderText: '',
          userMessage: 'Waiting for opponent to join'
        })

        console.log('state in parent : ', this.state.ready)

        firebase
          .database()
          .ref(`games/${result.data.gameId}/state`)
          .on('value', (snapshot) => {
            this.handleOpponentMove(snapshot.val());

            if (!this.state.ready && snapshot.val().user2) {
              console.log(this.state.ready)
              console.log('changing state')
              this.genericSetState({
                ready: true,
                frozen: false,
                userMessage: 'Your move..'
              })
            }
          });
      })
      .catch((e) => {
        console.log(e)
      });
  }

  joinGame = (gameNameInput) => {
    // Assign to variables as we'll be calling these
    // functions at places where `this` won't work
    const stateChanger = this.genericSetState
    const moveHandler = this.handleOpponentMove

    stateChanger({
      loaderText: 'Joining Game'
    })

    joinGameByName({
      gameName: gameNameInput
    }).then((result) => {
        // Read result of the Cloud Function.
        if (!result.data) {
          alert ('Game does not exist')

          stateChanger({
            loaderText: ''
          })
          return
        }
        stateChanger({
          gameId: result.data.gameId,
          gameName: result.data.gameName,
          userNo: 2,
          loaderText: ''
        })

        console.log(`listening to game : games/${result.data.gameId}/state`)

        firebase
          .database()
          .ref(`games/${result.data.gameId}/state`)
          .on('value', (snapshot) => {
            moveHandler(snapshot.val());

            if (!this.state.ready) {
              stateChanger({
                ready: true,
                userMessage: 'Waiting for opponent to play'
              })
            }
          });
      })
      .catch((e) => {
        console.log(e)
      });
  }

  clonedTiles = () => _.cloneDeep(this.state.tiles)

  changeTileText = (tileIds, toShow, isOpened, callback = () => {}) => {
    const tileId1 = tileIds[0]
    const tileId2 = tileIds[1]
    const tileCopy = this.clonedTiles()
    
    tileCopy[tileId1].text = toShow ? tileCopy[tileId1].value : ''
    tileCopy[tileId1].isSelected = false
    tileCopy[tileId2].text = toShow ? tileCopy[tileId2].value : ''
    tileCopy[tileId2].isSelected = false

    tileCopy[tileId1].isOpened = isOpened
    tileCopy[tileId2].isOpened = isOpened

    this.setState({
      tiles: tileCopy,
      userAlert: '',
      selectedTileCount: 0,
      openTileCount: isOpened ? this.state.openTileCount+2 : this.state.openTileCount 
    }, callback)
  }

  tileClickHandler = (tileId) => {
    // Return if the grid has been frozen
    if (this.state.frozen) return

    const tileCopy = this.clonedTiles()
    tileCopy[tileId].isSelected = !tileCopy[tileId].isSelected

    // Don't do anything if the tile is already open
    if (tileCopy[tileId].isOpened) return

    // Return if two tiles have already been selected
    // and another tile is trying to be selected
    if (this.state.selectedTileCount === 2 && tileCopy[tileId].isSelected) {
      this.setState({
        userAlert: 'You have already selected two tiles!'
      })
      return
    }

    const tileCountDifference = tileCopy[tileId].isSelected ? 1 : -1

    this.setState({
      tiles: tileCopy,
      selectedTileCount: this.state.selectedTileCount + tileCountDifference,
      userAlert: ''
    })
  }

  updateFirebase = (move, scores) => {
    const updates = {};
    updates[`games/${this.state.gameId}/state/totalTileCount`] = this.state.totalTileCount;
    updates[`games/${this.state.gameId}/state/tiles`] = this.state.tiles;
    updates[`games/${this.state.gameId}/state/selectedTileCount`] = this.state.selectedTileCount;
    updates[`games/${this.state.gameId}/state/openTileCount`] = this.state.openTileCount;
    updates[`games/${this.state.gameId}/state/user1Score`] = scores.user1Score;
    updates[`games/${this.state.gameId}/state/user2Score`] = scores.user2Score;
    updates[`games/${this.state.gameId}/state/prevMove/player`] = firebase.auth().currentUser.uid;
    updates[`games/${this.state.gameId}/state/prevMove/move`] = move;

    // Uncomment to debug
    // console.log(`Updating games/${this.state.gameId}/state with ${JSON.stringify(updates)}`)

    firebase
      .database()
      .ref()
      .update(updates)
      .then(() => {
        // Uncomment to debug
        // console.log(`Done with update`)
        return true
      })
  }

  performShowTile = (isMyTurn=false) => {
    const tiles = this.clonedTiles()
    const selectedTiles = _
        .chain(tiles)
        .values()
        .filter('isSelected')
        .value()

    let isOpened = false
    if (selectedTiles[0].value === selectedTiles[1].value) isOpened = true

    const newOpenTileCount = this.state.openTileCount + 2

    let userMessage = 'Waiting for opponent to play'
    if (newOpenTileCount === this.state.totalTileCount) {
      userMessage = 'Game Over!'
      this.setState({
        userMessage: userMessage
      })
    }
    if (isMyTurn) {
      // This check is used to avoid updating firebase when
      // we have received the opponent's move
      // If we update in this case, we will face an
      // infinite loop

      let newUser1Score = this.state.user1Score
      let newUser2Score = this.state.user2Score
      if (isOpened) {
        this.state.userNo === 1 ? newUser1Score++ : newUser2Score++
      }

      this.updateFirebase('opened', {
        user1Score: newUser1Score,
        user2Score: newUser2Score
      })

      this.setState({
        user1Score: newUser1Score,
        user2Score: newUser2Score,
        userMessage: userMessage
      })
    }

    const tileIds = _.map(selectedTiles, 'id')
    this.changeTileText(tileIds, true, isOpened)

    // Don't hide the tiles again if their values matched
    if (isOpened) {
      this.setState({
        frozen: !!isMyTurn
      })
      return
    }

    setTimeout(() => {
      this.changeTileText(tileIds, false, false, () => {
        this.setState({
          frozen: !!isMyTurn
        })
      })
    }, 2500)
  }

  showClickHandler = () => {
    // Return if the grid has been frozen
    if (this.state.frozen) return

    // Verify that two tiles have been selected
    if (this.state.selectedTileCount < 2) {
      this.setState({
        userAlert: 'You must select 2 tiles first!'
      })
      return
    }

    // Freeze the grid to avoid further clicks
    this.setState({
      frozen: true
    })

    this.performShowTile(true)
  }

  performSwapTile = (isMyTurn=false) => {
    const tiles = this.clonedTiles()
    const selectedTiles = _
        .chain(tiles)
        .values()
        .filter('isSelected')
        .value()

    const temp = selectedTiles[0].value
    selectedTiles[0].value = selectedTiles[1].value
    selectedTiles[1].value = temp

    selectedTiles[0].isSelected = false
    selectedTiles[1].isSelected = false

    if (isMyTurn) {
      // This check is used to avoid updating firebase when
      // we have received the opponent's move
      // If we update in this case, we will face an
      // infinite loop

      this.updateFirebase('swapped', {
        user1Score: this.state.user1Score,
        user2Score: this.state.user2Score
      })

      this.setState({
        userMessage: 'Waiting for opponent to play',
        swapsLeft: this.state.swapsLeft - 1
      })
    }

    // Set the new tiles
    this.setState({
      tiles: tiles,
      userAlert: '',
      frozen: !!isMyTurn,
      selectedTileCount: 0
    })
  }

  swapClickHandler = () => {
    // Return if the grid has been frozen
    if (this.state.frozen) return

    // Verify that two tiles have been selected
    if (this.state.selectedTileCount < 2) {
      this.setState({
        userAlert: 'You must select 2 tiles first!'
      })
      return
    }

    // Freeze the grid to avoid further clicks
    this.setState({
      frozen: true
    })

    this.performSwapTile(true)
  }

  renderGame () {
    // shareCode is used to determine whether we need to
    // share the code of a game that we created or
    // display to controls to start a new game or join
    // an existing one
    const shareCode = !!this.state.gameName

    return (
      <Container>
        <StartGame
          show={!this.state.ready}
          shareCode={shareCode}
          gameName={this.state.gameName}
          joinGameHandler={(gameNameInput) => this.joinGame(gameNameInput)}
          startGameHandler={() => this.startNewGame()}
          swapClickHandler={() => this.swapClickHandler()} />

        <Game
          show={this.state.ready}
          tiles={this.state.tiles}
          tileClickHandler={(tileId) => this.tileClickHandler(tileId)}
          prevMove={this.state.prevMove} 
          showClickHandler={() => this.showClickHandler()}
          swapClickHandler={() => this.swapClickHandler()}
          swapsLeft={this.state.swapsLeft}
          userNo={this.state.userNo}
          user1Score={this.state.user1Score}
          user2Score={this.state.user2Score} />

        <Message message={this.state.userMessage} alert={this.state.userAlert} />
      </Container>
    )
  }

  renderLoginControls = () => {
    return (
      <Login
        stateSetter={(newState) => this.genericSetState(newState)} />
    )
  }

  render () {
    return (
      <div className="welcome">
        <Container fluid>
          <Loader show={!!this.state.loaderText} text={this.state.loaderText} /> 

          <Header />

          <br/>

          <Container>
            { this.state.authenticated ? this.renderGame() : this.renderLoginControls() }
          </Container>
        </Container>
      </div>
    )
  }
}

export default Smurry
