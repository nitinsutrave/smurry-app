import * as firebase from "firebase/app";

import "firebase/auth"
import "firebase/database"
import "firebase/functions"
import "firebase/firestore"

const firebaseConfig = {
  apiKey: "AIzaSyAEMzxTP8VIp3jFjUZm7crXf51eiTj3RgA",
  authDomain: "smurry-e0e47.firebaseapp.com",
  databaseURL: "https://smurry-e0e47.firebaseio.com",
  projectId: "smurry-e0e47",
  storageBucket: "smurry-e0e47.appspot.com",
  messagingSenderId: "840961371022",
  appId: "1:840961371022:web:7291b682792c99a49592df",
  measurementId: "G-GMX9XCDHC8"
};

firebase.initializeApp(firebaseConfig);

export default firebase;
