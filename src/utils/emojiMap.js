
const emojiMap = {
  'A': 'https://res.cloudinary.com/nitinsutrave/image/upload/v1586502637/smurry/emoji-1.png',
  'B': 'https://res.cloudinary.com/nitinsutrave/image/upload/v1586503093/smurry/emoji-2.png',
  'C': 'https://res.cloudinary.com/nitinsutrave/image/upload/v1586503093/smurry/emoji-3.png',
  'D': 'https://res.cloudinary.com/nitinsutrave/image/upload/v1586503092/smurry/emoji-4.png',
  'E': 'https://res.cloudinary.com/nitinsutrave/image/upload/v1586503092/smurry/emoji-5.png',
  'F': 'https://res.cloudinary.com/nitinsutrave/image/upload/v1586503092/smurry/emoji-6.png',
  'G': 'https://res.cloudinary.com/nitinsutrave/image/upload/v1586503090/smurry/emoji-7.png',
  'H': 'https://res.cloudinary.com/nitinsutrave/image/upload/v1586503089/smurry/emoji-8.png',
  'I': 'https://res.cloudinary.com/nitinsutrave/image/upload/v1586503092/smurry/emoji-9.png',
  'J': 'https://res.cloudinary.com/nitinsutrave/image/upload/v1586503088/smurry/emoji-10.png',
  'K': 'https://res.cloudinary.com/nitinsutrave/image/upload/v1586503086/smurry/emoji-11.png',
  'L': 'https://res.cloudinary.com/nitinsutrave/image/upload/v1586503082/smurry/emoji-12.png'
}

export default emojiMap
