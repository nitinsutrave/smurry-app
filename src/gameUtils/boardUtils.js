import * as _ from 'lodash';

const masterList = ['A','B','C','D','E','F','G','H','I','J','K','L']

export const getRandomTiles = (boardSize) => {
  // pick random elements from the master list
  const picked = _.sampleSize(masterList, boardSize)

  // duplicate the list as we need two entries
  // of each element in the grid
  const tiles = _.concat(picked, picked)

  const randomTiles = []
  while (tiles.length) {
    const iToPull = _.random(tiles.length-1)
    randomTiles.push(_.pullAt(tiles, [iToPull, iToPull])[0])
  }

  return randomTiles
}
