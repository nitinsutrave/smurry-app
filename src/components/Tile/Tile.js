import React from 'react'

import emojiMap from './../../utils/emojiMap'
import './Tile.css'

const Tile = (props) => {
  return (
    <div 
      onClick={() => {props.onTileClick(props.id)}}
      className={`tile ${props.isSelected ? 'tile-selected' : ''}`}
      >
        {
          props.text ? <img alt="" src={emojiMap[props.text]}></img> : ''
        }
      </div>
  )
}

export default Tile;
