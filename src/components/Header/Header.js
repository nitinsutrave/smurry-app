import React from 'react';

import Container from 'react-bootstrap/Container';

import './Header.css';

const Header = () => {
  return (
    <Container>
      <h1 id="title">SMURRY</h1>
      <p id="subtitle"><i>Where memory meets strategy</i></p>
    </Container>
  )
}

export default Header;
