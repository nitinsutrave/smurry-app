import React from 'react';

import Button from 'react-bootstrap/Button';
import Container from 'react-bootstrap/Container';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';

const ShareCode = (props) => {
  return (
    <Container>
      <Container className="centred">
        <Row>
          <Col>Game Code :</Col>
          <Col><p>{props.gameName}</p></Col>
          <Col>
            <Button 
              variant="secondary" 
              size="sm"
              onClick={() => {
                const el = document.createElement('textarea');
                el.value = props.gameName;
                document.body.appendChild(el);
                el.select();
                document.execCommand('copy');
                document.body.removeChild(el);
              }}
            >Copy</Button>
            </Col>
        </Row>
      </Container>
      <br/>
    </Container>
  )
}

export default ShareCode;
