import React, { Component } from 'react';

import Button from 'react-bootstrap/Button';
import Container from 'react-bootstrap/Container';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';

class CreateOrJoin extends Component {
  constructor (props) {
    super ()

    this.startGameHandler = props.startGameHandler
    this.joinGameHandler = props.joinGameHandler

    this.state = {
      gameNameInput: ''
    }
  }

  onGameNameInputChange = (event) => {
    this.setState({
      gameNameInput: event.target.value
    })
  }

  render () {
    return (
      <Container>
        <Form>
          <Form.Group>
            <Form.Row>
              <Col>
                <Form.Group>
                  <Button
                    className="game-control-element button"
                    variant="primary"
                    onClick={() => {this.startGameHandler()}}
                  >Start New Game</Button>
                </Form.Group>
              </Col>
            </Form.Row>
          </Form.Group>

          <Form.Group>
            <Form.Row>
              <Col>
                <Form.Group>
                  <p>OR</p>
                </Form.Group>
              </Col>
            </Form.Row>
          </Form.Group>

          <Form.Group>
            <Form.Row>
              <Col>
                <Form.Group>
                  <Form.Control
                    className="game-control-element"
                    placeholder="Enter Game Code"
                    type="text"
                    onChange={(event) => {this.onGameNameInputChange(event)}}
                    value={this.state.gameNameInput} />
                </Form.Group>
              </Col>
            </Form.Row>

            <Form.Row>
              <Col>
                <Form.Group>
                  <Button
                    className="game-control-element button"
                    variant="primary"
                    onClick={() => {this.joinGameHandler(this.state.gameNameInput)}}
                  >Join Game</Button>
                </Form.Group>
              </Col>
            </Form.Row>
          </Form.Group>
        </Form>
      </Container>
    )
  }
}

export default CreateOrJoin
