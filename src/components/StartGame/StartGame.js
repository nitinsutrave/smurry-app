import React from 'react';

import ShareCode from './ShareCode';
import CreateOrJoin from './CreateOrJoin';

const shareCodeComponent = (props) =>  (
  <ShareCode gameName={props.gameName} />
)

const createOrJoinComponent = (props) => (
  <CreateOrJoin
    startGameHandler={props.startGameHandler}
    joinGameHandler={props.joinGameHandler} />
)

const StartGame = (props) => {
  if (!props.show) return null

  return ( 
    props.shareCode ? 
    shareCodeComponent(props) : 
    createOrJoinComponent(props)
  )
}

export default StartGame;
