import React from 'react';

import Button from 'react-bootstrap/Button';
import Container from 'react-bootstrap/Container';
import Col from 'react-bootstrap/Col';
import Row from 'react-bootstrap/Row';

import Board from '../Board/Board';

const Game = (props) => {
  if (!props.show) return null

  return (
    <Container className="game-area">
      <Container className="board">
        <Board 
          onTileClick={(tileId) => {props.tileClickHandler(tileId)}}
          tiles={props.tiles} />
      </Container>

      <Container>
        <Row>
          <Col sm={8}>
            <Container>
              <p>{ props.prevMove ? `Last move: ${props.prevMove}` : 'Select two tiles' }</p>
            </Container>

            <Container>
              <Row>
                <Col></Col>
                <Col>
                  <Button
                    className="button" 
                    onClick={() => {props.showClickHandler()}}
                  >Reveal</Button></Col>
                <Col>
                  <Button 
                    className="button" 
                    onClick={() => {props.swapClickHandler()}}
                    disabled={ props.swapsLeft > 0 ? false : true }
                  >Swap ({props.swapsLeft} left)</Button>
                </Col>
                <Col></Col>
              </Row>
            </Container>
          </Col>

          <Col sm={4}>
            <Container>
              <Row>
                <Col><p>{ `Your score     : ${props.userNo === 1 ? props.user1Score : props.user2Score}` }</p></Col>
              </Row>
              <Row>
                <Col><p>{ `Opponent score : ${props.userNo === 1 ? props.user2Score : props.user1Score}` }</p></Col>
              </Row>
            </Container>
          </Col>
        </Row>
      </Container>

    </Container>
  )
}

export default Game;
