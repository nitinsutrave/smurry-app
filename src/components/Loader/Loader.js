import React from 'react';

import Modal from 'react-bootstrap/Modal';
import Spinner from 'react-bootstrap/Spinner';

const Loader = (props) => {
  return (
    <Modal show={!!props.show} onHide={() => {}}>
      <Modal.Body>
        <Spinner animation="border" />
      </Modal.Body>

      <Modal.Body>{props.text}</Modal.Body>
    </Modal>
  )
}

export default Loader
