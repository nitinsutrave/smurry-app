import React from 'react'

import Tile from '../Tile/Tile'
import './Board.css'

const Board = (props) => {
  const tiles = props.tiles
  return Object.keys(tiles).map(key => <Tile 
  onTileClick={props.onTileClick}
  key={key} 
  id={key}
  isSelected={tiles[key].isSelected}
  text={tiles[key].text} />)
}

export default Board
