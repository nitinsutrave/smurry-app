import React, { Component } from 'react'
import firebase from './../../utils/firebase'

import Button from 'react-bootstrap/Button';
import Container from 'react-bootstrap/Container';
import Col from 'react-bootstrap/Col';
import Form from 'react-bootstrap/Form';

export default class Login extends Component {

  constructor (props) {
    super()

    this.gameStateSetter = props.stateSetter

    this.state = {
      email: '',
      password: ''
    }
  }

  onInputChanged (event, field) {
    this.setState({
      [field]: event.target.value
    })
  }

  onSignUp () {
    this.gameStateSetter({loaderText: 'Signing Up...'})
    firebase
      .auth()
      .createUserWithEmailAndPassword(this.state.email, this.state.password)
      .catch((error) => {
        // Handle Errors here.
        const errorMessage = error.message;
        
        // Uncomment to debug
        // const errorCode = error.code;
        // console.log(errorCode)

        this.gameStateSetter({loaderText: ''})

        alert(errorMessage)
      });
  }

  onSignIn () {
    this.gameStateSetter({loaderText: 'Signing In...'})
    firebase
      .auth()
      .signInWithEmailAndPassword(this.state.email, this.state.password)
      .catch((error) => {
        // Handle Errors here.
        const errorMessage = error.message;

        // Uncomment to debug
        // const errorCode = error.code;
        // console.log(errorCode)

        this.gameStateSetter({loaderText: ''})
        
        alert(errorMessage)
      });
  }

  render () {
    return (
      <Container>
        <Form>
        
          <Form.Row>
            <Col>
              <Form.Group controlId="formBasicEmail">
                  <Form.Control 
                    placeholder="emailid"
                    type="email"
                    onChange={(event) => {this.onInputChanged(event, 'email')}}
                    value={this.state.email} />
              </Form.Group>
            </Col>
          </Form.Row>

          <Form.Row>
            <Col>
              <Form.Group controlId="formBasicPassword">
                  <Form.Control 
                    placeholder="password"
                    type="password"
                    onChange={(event) => {this.onInputChanged(event, 'password')}}
                    value={this.state.password} />
              </Form.Group>
            </Col>
          </Form.Row>

          <Form.Row>
            <Col>
              <Form.Group>
                <Button
                  className="button"
                  onClick={() => {this.onSignIn()}}
                >
                  Sign In
                </Button>
              </Form.Group>
            </Col>

            <Col>
              <Form.Group>
                <Button
                  className="button"
                  onClick={() => {this.onSignUp()}}
                >
                  Sign Up
                </Button>
              </Form.Group>
            </Col>
          </Form.Row>
        </Form>
      </Container>
    )
  }
}
