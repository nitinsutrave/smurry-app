import React from 'react';

import Container from 'react-bootstrap/Container';

const Message = (props) => {
  return (
    <Container>
      <p><b>{props.message}</b></p>
      <p><b>{props.alert}</b></p>
    </Container>
  )
}

export default Message;
